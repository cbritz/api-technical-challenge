# Federal Foundry API Technical Challenge

## Overview
For this technical challenge, your task is to write an API (in the language of your choice) that can create, update, fetch, and remove entries from a database (of your choice). Seedable data is provided under `./data/vendors.json`.

## Requirements
- API with the appropriate http methods
  - fetch a list of vendors (paginated with 2 vendors per page)
  - fetch a single vendor (by your choice of identifier)
  - update a single vendor
  - create a new vendor
  - remove a single vendor
- responses in `json` format
- Database that is directly updated by your code
- `README.md` with instructions on how to run your code/database


## Notes
- **Authentication is _not_ required**
- This should take 1-2 hours to complete
